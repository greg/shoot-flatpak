#!/bin/bash

#//// GREG POUR LE GARAGE NUMERIQUE \\\\#
# script pour Debian 12 désinstallant VScodium et Firefox version Flatpak ainsi que Flatpak lui-même,
# et les réinstalle dans leur version Debian de base.

# FONCTION INSTALL VSCODIUM
install_vscodium() {
    URL="https://github.com/VSCodium/vscodium/releases/download/1.88.1.24104/codium_1.88.1.24104_amd64.deb"
    
    cd /tmp
    wget "${URL}"
    echo "Téléchargement de VSCodium ${VERSION} depuis GitHub..."

    sudo dpkg -i codium*.deb

    rm -rf codium*.deb
    cd -

    echo "VSCodium ${VERSION} a été installé avec succès. Enjoy!."
}

# FONCTION UNINSTALL FLATPAK STUFFS
uninstall_flatpak_stuffs() {
    if ! command --version flatpak &> /dev/null
    then
        echo "Flatpak est installé!!!"
        if flatpak list | grep -q 'com.vscodium.codium'
        then
            flatpak uninstall -y flathub com.vscodium.codium
            echo "VSCodium Flatpak version est desinstallé"
        fi
        if flatpak list | grep -q 'org.mozilla.firefox'
        then
            flatpak uninstall -y flathub org.mozilla.firefox
            echo "Firefox Flatpak version est desinstallé"
        fi
        sudo apt remove --purge flatpak -y
    else
        echo "Flatpak n'est pas installé, sauvé!"
    fi
}

# MAIN
echo "
  /$$$$$$   /$$$$$$  /$$$$$$$   /$$$$$$   /$$$$$$  /$$$$$$$$ /$$   /$$ /$$   /$$ /$$      /$$
 /$$__  $$ /$$__  $$| $$__  $$ /$$__  $$ /$$__  $$| $$_____/| $$$ | $$| $$  | $$| $$$    /$$$
| $$  \__/| $$  \ $$| $$  \ $$| $$  \ $$| $$  \__/| $$      | $$$$| $$| $$  | $$| $$$$  /$$$$
| $$ /$$$$| $$$$$$$$| $$$$$$$/| $$$$$$$$| $$ /$$$$| $$$$$   | $$ $$ $$| $$  | $$| $$ $$/$$ $$
| $$|_  $$| $$__  $$| $$__  $$| $$__  $$| $$|_  $$| $$__/   | $$  $$$$| $$  | $$| $$  $$$| $$
| $$  \ $$| $$  | $$| $$  \ $$| $$  | $$| $$  \ $$| $$      | $$\  $$$| $$  | $$| $$\  $ | $$
|  $$$$$$/| $$  | $$| $$  | $$| $$  | $$|  $$$$$$/| $$$$$$$$| $$ \  $$|  $$$$$$/| $$ \/  | $$
 \______/ |__/  |__/|__/  |__/|__/  |__/ \______/ |________/|__/  \__/ \______/ |__/     |__/
"
sudo apt-get update -y
uninstall_flatpak_stuffs
sudo apt-get install firefox-esr -y
install_vscodium